<?php
/*
Twando.com Free PHP Twitter Application
http://www.twando.com/
*/

include('inc/include_top.php');
include('inc/class/class.cron.php');

$cron = new cronFuncs();

//Defines
set_time_limit(0);
$run_cron = true;

//Check cron key and if running
if ( ($argv[1] != CRON_KEY) and ($_GET['cron_key'] != CRON_KEY) ) {
 echo mainFuncs::push_response(23);
 $run_cron = false;
} else {
 if ($cron->get_cron_state('tweet') == 1) {
  echo mainFuncs::push_response(24);
  $run_cron = false;
 }
}

if ($run_cron == true) {

 /*
 New to 0.3 - Some people on super cheap hosting seem to get
 SQL errors - output them if they occur
 */
 $db->output_error = 1;

 //Set cron status
 $cron->set_cron_state('tweet',1);

 //Get credentials
 $ap_creds = $db->get_ap_creds();

 //Loop through all accounts
 $q1 = $db->query("SELECT * FROM " . DB_PREFIX . "authed_users ORDER BY (followers_count + friends_count) ASC");
 while ($q1a = $db->fetch_array($q1)) {

  //Defines
  $connection = new \Abraham\TwitterOAuth\TwitterOAuth($ap_creds['consumer_key'], $ap_creds['consumer_secret'], $q1a['oauth_token'], $q1a['oauth_token_secret']);
  $cron->set_user_id($q1a['id']);
  $cron->set_log($q1a['log_data']);

 // El tiempo puede variar entre PHP y el servidor si la zona horaria del servidor no coincide con PHP zona horaria.
 // Todos los scripts PHP utilizan el tiempo en lugar de NOW () para evitar problemas.
  $current_date = date("Y-m-d");
  $current_time = date("H:i");
  //Get scheduled tweets for this user older than or equal to current time
  $q2 = $db->query("SELECT * FROM " . DB_PREFIX . "scheduled_tweets WHERE owner_id = '" . $db->prep($q1a['id']) . "' AND time_to_post != '0000-00-00' AND time_to_post <= '" . $db->prep($current_date) . "' AND time <= '" . $db->prep($current_time) . "' ORDER BY time_to_post, time ASC");
  //echo "SELECT * FROM " . DB_PREFIX . "scheduled_tweets WHERE owner_id = '" . $db->prep($q1a['id']) . "' AND time_to_post != '0000-00-00' AND time_to_post <= '" . $db->prep($current_date) . "' AND time <= '" . $db->prep($current_time) . "' ORDER BY time_to_post, time ASC<br>";
  while ($q2a = $db->fetch_array($q2)) {
     echo $q2a['imagen']."-> imagen </br>";
    if($q2a['imagen']!= ''){
     $media = $connection->upload('media/upload', array('media' => 'inc/upload/'.htmlspecialchars($q2a['imagen'])));

    $parameters = array(
    'status' => $q2a['tweet_content'],
    'media_ids' => $media->media_id_string,
);
 //envia tweets con imagenes
  $resul=$connection->post("statuses/update", array("status" => $parameters['status'], "media_ids" => $parameters['media_ids']));
  
    }else{
      
   //Post the tweet
   $connection->post('statuses/update', array('status' => $q2a['tweet_content']));
}
   //Log result - reasons for a non 200 include duplicate tweets, too many tweets
   //posted in a period of time, etc etc.
   if ($connection->getLastHttpCode() == 200) {
   
$cron->store_cron_log(2,$cron_txts[18] . $q2a['tweet_content'] . $cron_txts[19],$q2a['id_user'],'');
   } else {
    
$cron->store_cron_log(2,$cron_txts[18] . $q2a['tweet_content'] . $cron_txts[20],$q2a['id_user'],'');
   }

   //Delete the tweet si la fecha actual es menor que la fecha final
   if ($current_date < $q2a['fecha_fin']) {
// "aqui se suma un numero a time to post      
 $db->query(" UPDATE " . DB_PREFIX . "scheduled_tweets SET time_to_post= ADDDATE(time_to_post, INTERVAL 1 DAY) WHERE id = '" . $q2a['id'] . "' ");
   }else{
       // "deberia borrarse el registro de la tabla y la imagen
    $filename= 'inc/upload/'.htmlspecialchars($q2a['imagen']);
                         unlink($filename);   
   $db->query("DELETE FROM " . DB_PREFIX . "scheduled_tweets WHERE owner_id = '" . $db->prep($q1a['id']) . "' AND id = '" . $q2a['id'] . "'");
   }
  }

 //End of db loop
 }

 //Optimize tweet table
 $db->query("OPTIMIZE TABLE " . DB_PREFIX . "scheduled_tweets");

 //Set cron status
 $cron->set_cron_state('tweet',0);

 echo mainFuncs::push_response(32);

//End of run cron
}

include('inc/include_bottom.php');
?>
