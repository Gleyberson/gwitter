
    <script src="inc/style/js/bootstrap.min.js"></script>
  
 <!-- icheck -->
    <script src="inc/style/js/icheck/icheck.min.js"></script>
      <script src="inc/style/js/custom.js"></script>
    <!-- chart js -->
    <script src="inc/style/js/chartjs/chart.min.js"></script>

    <!-- bootstrap progress js -->
    <script src="inc/style/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="inc/style/js/nicescroll/jquery.nicescroll.min.js"></script>
       
    <!-- file input -->
    <script src="inc/scripts/fileinput.min.js" type="text/javascript"></script>
    <!-- daterangepicker & datetimepicker -->
    <script type="text/javascript" src="inc/style/js/moment.min2.js"></script>
    <script type="text/javascript" src="inc/style/js/datepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="inc/scripts/jquery.ui.core.min.js"></script>
    <script type="text/javascript" src="inc/scripts/jquery.ui.timepicker-es.js"></script>
    <script type="text/javascript" src="inc/scripts/jquery.ui.timepicker.js"></script>
    <!-- moris js 
    <script src="inc/style/js/moris/raphael-min.js"></script>
    <script src="inc/style/js/moris/morris.js"></script>
    <script src="inc/style/js/moris/example.js"></script>
      <!-- Datatables -->
            <script src="inc/style/js/datatables/js/jquery.dataTables.js"></script>
        <script src="inc/style/js/datatables/tools/js/dataTables.tableTools.js"></script>

<!--<div id="footer">
  &copy;<?=date("Y")?> <a href="http://www.telesurtv.net" target="_blank">teleSUR</a> (Version <?=TWANDO_VERSION?>) | <a rel="nofollow" href="https://twitter.com/gleyebrsonr" target="_blank">Telesur on twitter</a>
</div>

<!-- End of container -->



</body>
</html>
