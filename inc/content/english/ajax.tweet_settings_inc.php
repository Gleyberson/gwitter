<?php
/*
  Twando.com Free PHP Twitter Application
  http://www.twando.com/
 */

//Show content here
if ($_REQUEST['tab_id']) {
    // echo $_REQUEST['tab_id'];
    //Show content based on tab ID
    switch ($_REQUEST['tab_id']) {

        case 'tab1':
            ?>
            <div class="col-md-8 center-margin">
                <h2>Enviar un tweet</h2>
                <?= $response_msg ?>
                <?php
                if ($q1a['profile_image_url']) {
                    ?>
                    <!--<img src="<?= $q1a['profile_image_url'] ?>" style="float: right;" alt="<?= htmlentities($q1a['screen_name']) ?>" title="<?= htmlentities($q1a['screen_name']) ?>" />-->
                    <?php
                }
                ?>



                <form method="post" action="" enctype="multipart/form-data" class="form-horizontal form-label-left" name="quicktweet_form" id="quicktweet_form">
                    <div class="form-group">
                        <label>Enviar Tweet Rapido:</label>
                        <textarea name="tweet_content" id="tweet_content" class="form-control" required="required" style="height: 70px; width: 400px;" onkeyup="$('#count_box').val($('#tweet_content').val().length);"></textarea><br />
                    </div>
                    <div class="form-group">
                        <label>Characters:</label> 
                        <input type="text" disabled="disabled" name="count_box" id="count_box" size="3" value="0" class="input_box_style"/>
                    </div> 
                    <div class="col-md-5 col-sm-6 col-xs-12 form-group has-feedback center-margin">
                        <input id="file-1" name="file" type="file" class="file" >
                    </div>

                    <div class="form-group">
                        <br style="clear: both;" />
                        <input type="hidden" name="a" value="tweet" />
                        <input type="hidden" name="twitter_id" id="twitter_id" value="<?= $q1a['id'] ?>"/>
                        <input type="submit" value="Post Tweet" class="btn btn-primary" />
                </form>

            </div>    

            <h4 id='loading' ></h4>
            <div id="message"></div>
            </div>
            <?php
            break;
        case 'tab2':
            ?>

            <h2>Tweets Programados</h2>
            <?= $response_msg ?>
            Sus tweets programados actualmente se muestran a continuación:


            <table id="example" class="table table-striped responsive-utilities jambo_table">

                <thead>
                    <tr class="headings">
                        <th class="heading" width="190">Fecha Inicio</th>
                        <th class="heading">Fecha Final</th>
                        <th class="heading">Hora</th>
                        <th class="heading">Contenido del Tweet</th>     
                        <th class="heading">Imagen</th>
                        <th class="heading">Usuario</th>
                        <th class="heading" width="50">editar</th>
                        <th class="heading" width="20"><img src="inc/images/delete_icon.gif" width="14" height="15" /></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $current_date = date("Y-m-d");
                    $current_time = date("H:i");
                    $q2_base = "SELECT SQL_CALC_FOUND_ROWS * FROM " . DB_PREFIX . "scheduled_tweets WHERE owner_id='" . $db->prep($q1a['id']) . "' ORDER BY time_to_post, time ASC ";



//Pagination include
                    $js_page_func = 'ajax_tweet_settings_set_page';
                    $tab_id = 'tab2';
                    include('../ajax/ajax.pagination_inc.php');

                    if ($total_items > 0) {

                        $row_count = $db->num_rows($q2);
                        while ($q2a = $db->fetch_array($q2)) {

// llama el username por el id de la tabla usuarios
                            $q2_user = $db->query("SELECT * FROM " . DB_PREFIX . "usuarios WHERE id ='" . $q2a['id_user'] . "'");

                            $user = $db->fetch_array($q2_user);

//Check so pagination doesn't break when deleting
                            $page_switch = $page;
                            if ($row_count == 1) {
                                $page_switch = $page - 1;
                            }
                            if ($page_switch < 1) {
                                $page_switch = 1;
                            }
                            ?>

                            <?php
                            if (($_REQUEST['a'] == 'edittweet') and ( $_REQUEST['edittweet_id'] == $q2a['id'])) {
                                ?>
                                <tr>
                                    <td class=" ">
                                        <form method="post" action="" name="edittweetsave" id="edittweetsave"  onsubmit="javascript:ajax_tweet_settings_update('tab2', 'edittweetsave'); return false;">
                                            <input type="hidden" name="a" id="a" value="edittweetsave" />
                                            <input type="hidden" name="edittweetsave_id" id="edittweetsave_id" value="<?= $q2a['id'] ?>" />
                                            <?= $q2a['fecha_ini'] ?><input type="hidden" name="time_to_post" id="time_to_post"  value="<?= $q2a['time_to_post'] ?>" />
                                            <div style="display: none;"><input type="hidden" name="tweet_content" id="tweet_content" value="<?= $q2a['tweet_content'] ?>" /></div>
                                        </form>

                                    </td>
                                    <td class=" "><?= $q2a['fecha_fin'] ?></td>
                                    <td class=" "><?= $q2a['time'] ?></td>
                                    <td class=" ">
                                        <textarea name="tweet_content2" id="tweet_content2" class="input_box_style" style="height: 70px; width: 400px;" onkeyup="$('#count_box').val($('#tweet_content2').val().length); $('#tweet_content').val($('#tweet_content2').val());" onmouseout="$('#count_box').val($('#tweet_content2').val().length); $('#tweet_content').val($('#tweet_content2').val());" ><?= $q2a['tweet_content'] ?></textarea><br />
                                        Characters: <input type="text" name="count_box" id="count_box" size="3" disabled="disabled" value="<?= strlen($q2a['tweet_content']) ?>" class="input_box_style" style="width: 30px;"  />
                                    </td>
                                  <td class=" "><img width="150" height="150" src="inc/upload/<?= htmlspecialchars($q2a['imagen']) ?>"/></td>
                                  <td class=" "><?= $user['username'] ?></td>
                                   <td class=" "><a href="javascript:ajax_tweet_settings_update('tab2','edittweetsave');">Save</a></td>
                                    <td class=" "><a href="javascript:ajax_tweet_settings_del_tweet('<?= $q2a['id'] ?>','<?= $page_switch ?>');"><img src="inc/images/delete_icon.gif" width="14" height="15" /></a></td>
                                </tr>
                                </form>
                                <?php
                            } else {
                                ?>

                                <tr class="even pointer">
                                    <td class=" "><?= $q2a['fecha_ini'] ?></td>
                                    <td class=" "><?= $q2a['fecha_fin'] ?></td>
                                    <td class=" "><?= $q2a['time'] ?></td>
                                    <td class=" "><?= htmlspecialchars($q2a['tweet_content']) ?></td>
                                    <td class=" "><img width="150" height="150" src="inc/upload/<?= htmlspecialchars($q2a['imagen']) ?>"/></td>
                                    <td class=" "><?= $user['username'] ?></td>
                                    <td class=" "><a href="javascript:ajax_tweet_settings_edit_tweet_load('<?= $q2a['id'] ?>');">Edit</a></td>
                                    <td class=" "><a href="javascript:ajax_tweet_settings_del_tweet('<?= $q2a['id'] ?>','<?= $page_switch ?>');"><img src="inc/images/delete_icon.gif" width="14" height="15" /></a></td>
                                </tr>
                                <?php
                            }
                            ?>

                            <?php
                        }
                        ?>
                    </tbody>
                </table>

                <div class="dataTables_info">
                    <?= $total_items ?> result<?php if ($total_items != 1) {
                    echo 's';
                } ?> found; showing page <?= $page ?> of <?= $total_pages ?>.
                </div>
                <div class="dataTables_paginate paging_full_numbers">
                    <?php
                    if ($total_pages > 1) {
                        ?>
                        <?= $back_string ?>
                        <span> <?= $page_list_string ?></span>
                        <?= $next_string ?>
                        <?php
                    } else {
                        ?>
                        <div class="boxw">&nbsp;</div>
                        <?php
                    }
                    ?>
                </div>

                <?php
//If total items > 0
            } else {
                ?>
                <tr>
                    <td colspan="8" style="text-align: center">No hay tuits actualmente programados</td>
                </tr>
                </tbody>
                </table>
                <?php
//End of 0 total items
            }
            ?>
            <br /><br />
            * Tweets se publican en base a su tiempo los servidores de PHP, que podría no ser su hora local. La fecha de PHP actual y el tiempo es <?= date(TIMESTAMP_FORMAT) ?>.


            <?php
            break;
        case 'tab3':
            ?>
            <div class="col-md-8 center-margin">
                <h2>Programar a Tweet</h2>
            <?= $response_msg ?>

                <form method="post" action="" enctype="multipart/form-data" name="scheduletweet_form" id="scheduletweet_form" class="form-horizontal form-label-left input_mask">
                    <div class="form-group">
                        <label>Contenido de Tweet:</label>
                        <textarea name="tweet_content" id="tweet_content" class="form-control" required="required" style="height: 70px; width: 400px;" onkeyup="$('#count_box').val($('#tweet_content').val().length);"></textarea><br />
                    </div>
                    <div class="form-group">
                        <label>Characters:</label> 
                        <input type="text" name="count_box" disabled="disabled" id="count_box" size="3" value="0" class="input_box_style"/> 
                    </div>
                    <div class="col-md-5 col-sm-6 col-xs-12 form-group has-feedback center-margin">
                        <input id="file-1" name="file" type="file" class="file" >
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">

                        <div class="controls">
                            <input type="text" for="ex3" name="time" id="timepicker"  value="<?= date('H:i', time() + 3600) ?>" class="form-control has-feedback-left"/> 
                            <span class="fa fa-clock-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- gruiz --> 

                        <div class="control-group">
                            <div class="controls">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" for="ex3" style="width: 200px" name="date" id="reservation" class="form-control" value="<?php echo date('Y-m-d') ?> / <?php echo date('Y-m-d') ?>" />
                                </div>
                            </div>
                        </div>

                        <!--/gruiz-->
                    </div>

                    <div class="form-group"> 

                        <br style="clear: both;" />
                        <input type="hidden" name="a" value="scheduletweet" />
                        <input type="hidden" name="twitter_id" id="twitter_id" value="<?= $q1a['id'] ?>" />
                        <input type="submit" value="Programar Tweet" class="btn btn-primary" />   

                </form>
                <h4 id='loading' ></h4>
                <div id="message"></div>
                <br /><br />
                * Tweets se publican en base a su tiempo los servidores de PHP, que podría no ser su hora local. The current PHP date and time is <?= date(TIMESTAMP_FORMAT) ?>.
                <br />
            </div>
            </div>
            <?php
            break;
        case 'tab4':
            ?>
            <div class="col-md-6 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Configurar lista</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <!-- gruiz -->
                        <form method="post" action="" enctype="multipart/form-data" name="listconfig_form" id="listconfig_form" class="form-horizontal form-label-left input_mask">

                            <label>Activar lista:</label>
                            <p>
                                Activada:
                                <input type="radio" class="flat" name="activar" id="act" value="1" /> Desactivada:
                                <input type="radio" class="flat" name="activar" id="desac" value="0"  required  />
                            </p>
                            <label>Todos los dias:</label>
                            <p>
                                Si:
                                <input type="radio" class="allday" name="allday" id="alldayS" value="1"   /> No:
                                <input type="radio" class="allday" name="allday" id="alldayN" value="0" checked="true"/>
                            </p>

                          

                               
     <?php              

$qg = $db->query("SELECT * FROM tw_list_settings WHERE id='1'");
 $reg = $db->fetch_array($qg);
  
if($reg['all_day']==0){
    
    $alldayS= "false";
    $alldayN= "true";
}else{$alldayS= "true";$alldayN= "false";}
         if($reg['activa']==0){
                            echo "<script>"
             . "        // Check
document.getElementById('act').checked = false;
document.getElementById('alldayS').checked = ".$alldayS.";
document.getElementById('alldayN').checked = ".$alldayN.";
 $('#alldayS').prop('disabled', true);
                                             $('#alldayN').prop('disabled', true);
                                              $('#reservation').prop('disabled', ".$alldayS.");
                                              $('#heard').prop('disabled', true);

//  Uncheck
document.getElementById('desac').checked = true;  "
                                    . "</script> ";
   
         }
         else{
                            echo "<script>"
             . "        // Check
document.getElementById('act').checked = true;
document.getElementById('alldayS').checked = ".$alldayS.";
document.getElementById('alldayN').checked = ".$alldayN.";
                                            $('#alldayS').prop('disabled', false);
                                            $('#alldayN').prop('disabled', false);
                                            $('#reservation').prop('disabled', ".$alldayS.");
                                            $('#heard').prop('disabled', false);

//  Uncheck
document.getElementById('desac').checked = false;  "
                                    . "</script> ";  
         }
        
?>
<script>
                                $(document).ready(function () {
                                    $('input:radio[name=allday]').change(function () {
                                        if (this.value == '1') {
                                            $("#reservation").prop("disabled", true);
                                        } else if (this.value == '0') {
                                            $("#reservation").prop("disabled", false);
                                        }
                                    });
                                       $('input:radio[name=activar]').change(function () {
                                        if (this.value == '0') {
                                            $("#alldayS").prop("disabled", true);
                                             $("#alldayN").prop("disabled", true);
                                              $("#reservation").prop("disabled", true);
                                              $("#heard").prop("disabled", true);
                                             
                                        } else if (this.value == '1') {
                                            $("#alldayS").prop("disabled", false);
                                            $("#alldayN").prop("disabled", false);
                                            $("#reservation").prop("disabled", false);
                                            $("#heard").prop("disabled", false);
                                           
                                        }
                                    });
                                });
                            </script> 
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-prepend input-group">
                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                        <input type="text" for="ex3" style="width: 200px" name="date" id="reservation" class="form-control" value="<?php echo substr($reg['time_to_post'], 0, 10) ?> / <?php echo $reg['fecha_fin'] ?>" />
                                    </div>
                                </div>
                            </div>
                            <label for="heard">Frecuencia:</label>
                            <select id="heard" class="form-control" name="frecuencia" style="width: 200px" required>
                                <option value="">Selecciona..</option>
                                <option value="10">1 Tuit cada 10 Minutos</option>
                                <option value="15">1 Tuit cada 15 Minutos</option>
                                <option value="30">1 Tuit cada 30 Minutos</option>
                                <option value="60">1 Tuit cada 1 Hora</option>
                            </select>
                            <br style="clear: both;" />
                            <input type="submit" value="Guardar" id="guardar" class="btn btn-primary" />   
                        </form>
                           <h4 id='loading1' ></h4>
                        <div id="message1"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Añadir Tweet a la lista</h2>
                        <div class="clearfix"></div>
                    </div>
            <?= $response_msg ?>
                    <div class="x_content">
                        <form method="post" action="" enctype="multipart/form-data" name="schedulelist_form" id="schedulelist_form" class="form-horizontal form-label-left input_mask">
                            <div class="form-group">
                                <label>Contenido de Tweet:</label>
                                <textarea name="tweet_content" id="tweet_content" class="form-control" required="required" style="height: 70px; width: 400px;" onkeyup="$('#count_box').val($('#tweet_content').val().length);"></textarea><br />
                            </div>
                            <div class="form-group">
                                <label>Characters:</label> 
                                <input type="text" name="count_box" disabled="disabled" id="count_box" size="3" value="0" class="input_box_style"/> 
                            </div>
                            <div class="col-md-5 col-sm-6 col-xs-12 form-group has-feedback center-margin">
                                <input id="file-1" name="file" type="file" class="file" >
                            </div>


                            <div class="form-group"> 

                                <br style="clear: both;" />
                                <input type="hidden" name="a" value="scheduletweet" />
                                <input type="hidden" name="twitter_id" id="twitter_id" value="<?= $q1a['id'] ?>" />
                                <input type="submit" value="Programar Tweet"  class="btn btn-primary" />   

                        </form>
                        <h4 id='loading' ></h4>
                        <div id="message"></div>
                        <br /><br />
                        * Tweets se publican en base a su tiempo los servidores de PHP, que podría no ser su hora local. The current PHP date and time is <?= date(TIMESTAMP_FORMAT) ?>.
                        <br />
                    </div>
                </div>
            </div>
            </div>
            <?php
            break;
        case 'tab5':
            ?>

            <h2>Lista de Tweets</h2>
            <?= $response_msg ?>
            <?php
            $q2_con = $db->query("SELECT * FROM " . DB_PREFIX . "list_settings");

            $conf = $db->fetch_array($q2_con);
            if($conf['activa']==0){
                echo "La lista no está activada en este momento<br>";
            }else{echo "El proximo tweet sera enviado a  las: ".$conf['time_to_post']."<br>";}
                            ?>
            
            Sus tweets programados actualmente se muestran a continuación:


            <table id="example" class="table table-striped responsive-utilities jambo_table">

                <thead>
                    <tr class="headings">
                        <th class="heading">Fecha de Publicacíon</th>
                        <th class="heading">Contenido del Tweet</th>     
                        <th class="heading">Imagen</th>
                        <th class="heading">Creado por</th>
                        <th class="heading" width="50">editar</th>
                        <th class="heading" width="20"><img src="inc/images/delete_icon.gif" width="14" height="15" /></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $current_date = date("Y-m-d");
                    $current_time = date("H:i");
                    $q2_base = "SELECT SQL_CALC_FOUND_ROWS * FROM " . DB_PREFIX . "lists WHERE owner_id='" . $db->prep($q1a['id']) . "' ORDER BY time_to_post";

                    $q2 = $db->query($q2_base);
                    list($total_items) = $db->fetch_row($db->query("SELECT FOUND_ROWS();"));

                    if ($total_items > 0) {

                        $row_count = $db->num_rows($q2);
                        while ($q2a = $db->fetch_array($q2)) {

                            // llama el username por el id de la tabla usuarios
                            $q2_user = $db->query("SELECT * FROM " . DB_PREFIX . "usuarios WHERE id ='" . $q2a['id_user'] . "'");

                            $user = $db->fetch_array($q2_user);

                            //Check so pagination doesn't break when deleting
                            $page_switch = $page;
                            if ($row_count == 1) {
                                $page_switch = $page - 1;
                            }
                            if ($page_switch < 1) {
                                $page_switch = 1;
                            }
                            ?>

                            <?php
                            if (($_REQUEST['a'] == 'edittweet') and ( $_REQUEST['edittweet_id'] == $q2a['id'])) {
                                ?>
                                <tr>
                                    <td class=" ">
                                        <form method="post" action="" name="edittweetsave" id="edittweetsave"  onsubmit="javascript:ajax_tweet_settings_update('tab5', 'edittweetsave'); return false;">
                                            <input type="hidden" name="a" id="a" value="edittweetsave" />
                                            <input type="hidden" name="edittweetsave_id" id="edittweetsave_id" value="<?= $q2a['id'] ?>" />
                                            <?= $q2a['time_to_post'] ?><input type="hidden" name="time_to_post" id="time_to_post"  value="<?= $q2a['time_to_post'] ?>" />
                                            <div style="display: none;"><input type="hidden" name="tweet_content" id="tweet_content" value="<?= $q2a['tweet_content'] ?>" /></div>
                                        </form>

                                    </td>
                                    <td class=" ">
                                        <textarea name="tweet_content2" id="tweet_content2" class="input_box_style" style="height: 70px; width: 400px;" onkeyup="$('#count_box').val($('#tweet_content2').val().length); $('#tweet_content').val($('#tweet_content2').val());" onmouseout="$('#count_box').val($('#tweet_content2').val().length); $('#tweet_content').val($('#tweet_content2').val());" ><?= $q2a['tweet_content'] ?></textarea><br />
                                        Characters: <input type="text" name="count_box" id="count_box" size="3" disabled="disabled" value="<?= strlen($q2a['tweet_content']) ?>" class="input_box_style" style="width: 30px;"  />
                                    </td>
                                    <td class=" "><img width="150" height="150" src="inc/upload/<?= htmlspecialchars($q2a['imagen']) ?>"/></td>
                                    <td class=" "><?= $user['username'] ?></td>
                                    <td class=" "><a href="javascript:ajax_tweet_settings_update('tab5','edittweetsave');">Save</a></td>
                                    <td class=" "><a href="javascript:ajax_tweet_settings_del_tweet2('<?= $q2a['id'] ?>','<?= $page_switch ?>');"><img src="inc/images/delete_icon.gif" width="14" height="15" /></a></td>
                                </tr>
                                </form>
                                <?php
                            } else {
                                ?>

                                <tr class="even pointer">
                                    <td class=" "><?= $q2a['time_to_post'] ?></td>
                                    <td class=" "><?= htmlspecialchars($q2a['tweet_content']) ?></td>
                                    <td class=" "><img width="150" height="150" src="inc/upload/<?= htmlspecialchars($q2a['imagen']) ?>"/></td>
                                    <td class=" "><?= $user['username'] ?></td>
                                    <td class=" "><a href="javascript:ajax_tweet_settings_edit_tweet_load2('<?= $q2a['id'] ?>');">Edit</a></td>
                                    <td class=" "><a href="javascript:ajax_tweet_settings_del_tweet2('<?= $q2a['id'] ?>','<?= $page_switch ?>');"><img src="inc/images/delete_icon.gif" width="14" height="15" /></a></td>
                                </tr>
                                <?php
                            }
                            ?>

                            <?php
                        }
                        ?>
                    </tbody>
                </table>

                <div class="dataTables_info">
                    <?= $total_items ?> result<?php if ($total_items != 1) {
                        echo 's';
                    } ?> found; showing page <?= $page ?> of <?= $total_pages ?>.
                </div>
                <div class="dataTables_paginate paging_full_numbers">
                    <?php
                    if ($total_pages > 1) {
                        ?>
                        <?= $back_string ?>
                        <span> <?= $page_list_string ?></span>
                        <?= $next_string ?>
                        <?php
                    } else {
                        ?>
                        <div class="boxw">&nbsp;</div>
                    <?php
                }
                ?>
                </div>

                <?php
//If total items > 0
            } else {
                ?>
                <tr>
                    <td colspan="8" style="text-align: center">No hay tuits actualmente programados</td>
                </tr>
                </tbody>
                </table>
                <?php
//End of 0 total items
            }
            ?>
            <br /><br />
            * Tweets se publican en base a su tiempo los servidores de PHP, que podría no ser su hora local. La fecha de PHP actual y el tiempo es <?= date(TIMESTAMP_FORMAT) ?>.

            <?php
            break;
        //End of switch
    }

    //End of if $_REQUEST['tab_id']
}
?>
<script type="text/javascript">
//prueba enviar form
    $(document).ready(function (e) {
        $("#quicktweet_form").on('submit', (function (e) {
            e.preventDefault();
            $("#message").empty();
            $('#loading').show();
            $.ajax({
                url: "inc/ajax/procesa_quick.php", // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    $('#loading').hide();
                    $("#message").html(data);
                }
            });
        }));
//fin publicar rapido
        $("#scheduletweet_form").on('submit', (function (e) {
            e.preventDefault();
            $("#message").empty();
            $('#loading').show();
            $.ajax({
                url: "inc/ajax/procesa_schedule.php", // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    $('#loading').hide();
                    $("#message").html(data);
                }
            });
        }));
    });
//fin prueba enviar form

        $("#schedulelist_form").on('submit', (function (e) {
            e.preventDefault();
            $("#message").empty();
            $('#loading').show();
         
            $.ajax({
                url: "inc/ajax/procesa_conf.php", // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    $('#loading').hide();
                    $("#message").html(data);
       
                }
            });
        }));
// fin form para listas
        $("#listconfig_form").on('submit', (function (e) {
            e.preventDefault();
            $("#message1").empty();
            $('#loading1').show();
         
            $.ajax({
                url: "inc/ajax/procesa_config2.php", // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    $('#loading1').hide();
                    $("#message1").html(data);
       
                }
            });
        }));
// fin form para configurar la lista
    $(document).ready(function () {
        $("#file-1").fileinput({
            showCaption: false,
            browseLabel: "Imagen",
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            removeClass: "btn btn-danger",
            showUpload: false,
            browseClass: "btn btn-default btn-file",
            fileType: "any"
        });
        $('#timepicker').timepicker({
            showPeriodLabels: false,
        });
        $('#reservation').daterangepicker({
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Deciembre"
                ],
                "firstDay": 1
            },
            "startDate": '<?php echo date('m/d/Y') ?>',
            "endDate": '<?php echo date('m/d/Y') ?>'
        }, function (start, end, label) {
            $('#reservation').val(start.format('YYYY-MM-DD').toString() + " / " + end.format('YYYY-MM-DD').toString());
        });
    });

</script>



