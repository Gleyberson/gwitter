<?php
/*
Twando.com Free PHP Twitter Application
http://www.twando.com/
*/
//echo "Usuario conectado = ".$_SESSION['twando_username'];
if (!$content_id) {
exit;
}
global $db, $ap_creds;

if ((int)$_GET['msg']>0) {
 $response_msg = mainFuncs::push_response((int)$_GET['msg']);

}
?>
<?php
if ((!$ap_creds['consumer_key']) or ( !$ap_creds['consumer_secret'])) {
    if ($_SESSION['perfil'] == '2') {
        include('ini.menu.admin.php');
    } else {
        include('ini.menu.php');
    }
    ?>


    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Inicio</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>Inicio</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <!-- css -->
                        <h2>Register Your Application</h2>
                        Antes de poder empezar a utilizar Twando, primero debe registrar su aplicación con Twitter.
                        <ol>

                            <li>First, <a href="install_tables.php">click here to install the MySQL tables</a> if you have not done so already.</li>
                            <li>Next, visit <a href="https://dev.twitter.com/apps/new" target="_blank">https://dev.twitter.com/apps/new</a>;  you will need to sign in to your Twitter account to register an app.</li>
                            <li>Enter the values as demonstrated in <a href="inc/images/reg_ap.jpg" target="_blank">this picture</a>; your application URL is <b><?= BASE_LINK_URL ?></b>; your callback URL is <b><?= BASE_LINK_URL . 'callback.php' ?></b>.</li>
                            <li>You will then be given a consumer key and consumer secret (<a href="inc/images/reg_ap2.jpg" target="_blank">example</a>). Enter these in the boxes below to complete the setup of your Twitter app.</li>
                            <li>You must also make sure your application is set with <b>read/write access</b> on the settings page; <a href="inc/images/reg_ap3.jpg" target="_blank">click here to view</a>.</li>
                        </ol>
                        <form method="post" class="form-horizontal form-label-left" action="<?= BASE_LINK_URL ?>">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="clave">Consumer Key:<span class="required">*</span>
                                </label>       
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <input type="text" name="consumer_key" size="20" value="<?= $_POST['consumer_key'] ?>" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="clave">
                                    Consumer Secret:<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="consumer_secret" size="20" class="form-control col-md-7 col-xs-12" value="<?= $_POST['consumer_secret'] ?>"   />
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                    <input type="submit" value="Save Values!" name="login" class="btn btn-primary" />
                                    <input type="hidden" name="a" value="savekeys2" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
<?php
} else {
    if($_SESSION['perfil']== '2'){       
       include('ini.menu.admin.php'); 
    }else{
    include('ini.menu.php');}
    ?>
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Inicio</h3>
                        </div>
  
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="height:600px;">
                                <div class="x_title">
                                    <h2>Inicio</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div id="twitter_user_table">
                                <!-- aqui se muestra el contenido de ajax.index.php-->                                
                            </div>
                                <!-- gruiz -->
                                <?php
            $q1= $db->query("select * from tw_ap_settings");
                                    $row1= $db->num_rows($q1);
                                   $q= $db->query("select * from tw_authed_users");
                                    $row= $db->num_rows($q); 
                             
                                    if ($row == 0) {
                                    ?>                      
                                <h2>Application Details</h2>
Su solicitud ha sido registrada con una clave de cliente y un secreto del consumidor. Si desea actualizar estas credenciales, por favor haga clic aquí. <a href="javascript:void(0);" onclick="$('#cred_update_div').toggle();">click here</a>.
<div id="cred_update_div" style="display: none;">
<form method="post" action="<?=BASE_LINK_URL?>">
Consumer Key:<br />
<input type="text" name="consumer_key" size="20" class="input_box_style" value="<?=$ap_creds['consumer_key']?>" style="width: 350px;" />
<br />
Consumer Secret:<br />
<input type="text" name="consumer_secret" size="20" class="input_box_style" value="<?=$ap_creds['consumer_secret']?>" style="width: 350px;"  />
<br />
<input type="submit" value="Save Values!" name="login" id="login" class="submit_button_style" />
<input type="hidden" name="a" value="savekeys2" />
</form>
</div>
<h2>Authorized Twitter Accounts</h2> 
<?php
 if ($response_msg) {
     echo $response_msg;
  echo '<div id="index_resp_msg">' . $response_msg . '</div>';
 }
?>
<div id="twitter_user_table">

</div>
To authorize another account, make sure you are either signed out of all accounts or signed into the account you want to authorize on <a href="https://twitter.com/" target="_blank">Twitter</a> before clicking the button below.
<br />
<a href="redirect.php"><img src="inc/images/twitter_sign_in.jpg" style="margin: 5px 0px 0px 0px" alt="Sign in with Twitter" width="384" height="63" /></a>

<?php
//End of application registered
}
}
?>
                                <!-- gruiz -->
                        </div>
                    </div>

                </div>
<?php
//fin menu
include('fin.menu.php');

?>
  

