<?php
/*
Twando.com Free PHP Twitter Application
http://www.twando.com/
*/

//English response list
$response_msgs = array();
$response_msgs[1] = array('text'=>'Cuenta autorizada con éxito','type'=>'success');
$response_msgs[2] = array('text'=>'Se ha producido un error al intentar autorizar esa cuenta','type'=>'error');
$response_msgs[3] = array('text'=>'No se puede conectar a Twitter. Por favor, compruebe su consumer key and consumer secret values','type'=>'error');
$response_msgs[4] = array('text'=>'Cuenta eliminada','type'=>'success');
$response_msgs[5] = array('text'=>'Su combinación de nombre de usuario / contraseña es incorrecta. Por favor, inténtelo de nuevo','type'=>'error');
$response_msgs[6] = array('text'=>'Debe estar registrado para acceder a esta página!','type'=>'error');
$response_msgs[7] = array('text'=>'Inválida Twitter ID especificado. No se puede establecer opciones','type'=>'error');
$response_msgs[8] = array('text'=>'Ajustes guardados','type'=>'success');
$response_msgs[9] = array('text'=>'User details (screen name, profile image, follower counts etc) refreshed','type'=>'success');
$response_msgs[10] = array('text'=>'Twitter IDs Seleccionados borrados','type'=>'success');
$response_msgs[11] = array('text'=>'Los usuarios de Twitter seleccionados añadidos a la lista','type'=>'success');
$response_msgs[12] = array('text'=>'No se puede conectar a Twitter para actualizar detalles','type'=>'error');
$response_msgs[13] = array('text'=>'Tweet publicado con éxito.','type'=>'success');
$response_msgs[14] = array('text'=>'No se puede enviar Tweet.','type'=>'error');
$response_msgs[15] = array('text'=>'No se pudo conectar a la base de datos MySQL . Por favor, compruebe la configuración de conexión.','type'=>'error');
$response_msgs[16] = array('text'=>'Tweet programado eliminado.','type'=>'success');
$response_msgs[17] = array('text'=>'Tweet programado editado.','type'=>'success');
$response_msgs[18] = array('text'=>'Tweet programado.','type'=>'success');
$response_msgs[19] = array('text'=>'No hay filas CSV extraído correctamente tweets programadas.','type'=>'error');
$response_msgs[20] = array('text'=>'CSV upload successful. Tweets added to database','type'=>'success');
$response_msgs[21] = array('text'=>'mensaje de auto DM actualiza','type'=>'success');
$response_msgs[22] = array('text'=>'Cron registros purgados','type'=>'success');
$response_msgs[23] = array('text'=>'Invalid cron key','type'=>'error');
$response_msgs[24] = array('text'=>'Cron ya se está ejecutando','type'=>'error');
$response_msgs[25] = array('text'=>'No se encontraron usuarios para esa búsqueda . Ya podría estar siguiendo todos los resultados , o es posible que haya superado sus límites de la API','type'=>'error');
$response_msgs[26] = array('text'=>'Todos los usuarios seleccionados siguieron con éxito','type'=>'success');
$response_msgs[27] = array('text'=>'Algunos usuarios seleccionados siguieron con éxito, pero la API rechazaron algunas de las solicitudes de seguimiento','type'=>'success');
$response_msgs[28] = array('text'=>'Todas las solicitudes de seguimiento rechazadas por la API','type'=>'error');
$response_msgs[29] = array('text'=>' Se requieren al menos 2 authed cuentas de Twitter para realizar funciones múltiples de la cuenta','type'=>'error');
$response_msgs[30] = array('text'=>'Follow/unfollow requests sent to API','type'=>'success');
$response_msgs[31] = array('text'=>'Tweet mensajes enviados a la API','type'=>'success');
$response_msgs[32] = array('text'=>'Cron run complete','type'=>'success');
$response_msgs[33] = array('text'=>'Twando era incapaz de mirar hacia arriba dichos usuarios. Esto podría ser un problema temporal con la API de Twitter , o podría no existir los usuarios','type'=>'error');

//English cron text
$cron_txts = array();
$cron_txts[1] = 'API error . Todos siguen las operaciones desactivadas para esta pasada.';
$cron_txts[2] = 'seguidor';
$cron_txts[3] = 'seguidores';
$cron_txts[4] = 'amigo';
$cron_txts[5] = 'amigos';
$cron_txts[6] = ' se encuentra en primera pase.';
$cron_txts[7] = ' nuevos usuarios siguieron esta cuenta desde la última actualización.';
$cron_txts[8] = ' usuarios unfollowed esta cuenta desde la última actualización.';
$cron_txts[9] = ' nuevos usuarios seguidos por esta cuenta desde la última actualización.';
$cron_txts[10] = ' unfollowed usuarios por esta cuenta desde la última actualización.';
$cron_txts[11] = 'usuarios';
$cron_txts[12] = 'usuario';
$cron_txts[13] = ' unfollowed automatically.';
$cron_txts[14] = ' Seguido de nuevo de forma automática.';
$cron_txts[15] = 'DM';
$cron_txts[16] = 'DMs';
$cron_txts[17] = ' enviado automáticamente a los nuevos seguidores .';
$cron_txts[18] = 'Tweet "';
$cron_txts[19] = '" publicado con éxito .';
$cron_txts[20] = '" ha fallado la publicación ';

?>
