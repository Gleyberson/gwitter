<?php
/*
Twando.com Free PHP Twitter Application
http://www.twando.com/
*/

if (!$content_id) {
exit;
}
global $error_array, $return_url;
?>


  <div id="wrapper">
    
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

       <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form method="post" action="<?=BASE_LINK_URL?>">
                        <h1>Formulario de inicio</h1>
                        <?php
if ($error_array['message'] != "") {
 echo $error_array['message'] ;
} else {
 echo mainFuncs::push_response(6)."<br>" ;
}
?>
                        <div>                            
                            <input type="text" name="username_login" size="20" class="form-control" placeholder="Usuario" required="" value="<?=$_POST['username_login']?>" />
                        </div>
                        <div>                            
                            <input type="password" name="password_login" size="20" class="form-control" placeholder="Contraseña" required="" value="<?=$_POST['password_login']?>" />
                        </div>
                        <div>
                            <input type="submit" value="Log in" name="login" class="btn btn-default submit"/>
                            <input type="hidden" name="a" value="login2" />
                            <input type="hidden" name="return_url" value="<?=$return_url?>" />
                            <a class="reset_pass" href="#">Lost your password?</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">New to site?
                                <a href="#" class="to_register"> Create Account </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-twitter"></i> Programador de Twitter</h1>

                                <p>&copy;<?=date("Y")?> <a href="http://www.telesurtv.net" target="_blank">teleSUR</a> (Version <?=TWANDO_VERSION?>) | <a rel="nofollow" href="https://twitter.com/gleyebrsonr" target="_blank">Telesur on twitter</a></p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
       </div>
  </div>

