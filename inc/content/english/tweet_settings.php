<?php
/*
Twando.com Free PHP Twitter Application
http://www.twando.com/
*/

if (!$content_id) {
exit;
}
global $q1a, $pass_msg;
?>

<?php
if ($q1a['id'] == "")  {
 echo mainFuncs::push_response(7);
} else {
  if($_SESSION['perfil']== '2'){     
     include('ini.menu.admin.php'); 
    }else{       
    include('ini.menu.php');}
//List all options here
?>

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Tweets</h3>
                        </div>
  
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Ajustes de Tweet para <?=htmlentities($q1a['screen_name'])?></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="javascript:ajax_tweet_settings_tab('tab1');">Publicar Tweet Rapido</a>
                                            </li>
                                            <li role="presentation" class=""> <a href="javascript:ajax_tweet_settings_tab('tab2');">Tweets Programados</a>
                                            </li>
                                            <li role="presentation" class=""><a href="javascript:ajax_tweet_settings_tab('tab3');">Programar Tweets</a>
                                            </li>
                                            <li role="presentation" class=""><a href="javascript:ajax_tweet_settings_tab('tab4');">Configurar Lista</a>
                                            </li>
                                            <li role="presentation" class=""><a href="javascript:ajax_tweet_settings_tab('tab5');">Lista Programada</a>
                                            </li>
                                        </ul>
                                    <script>
                                         $(document).ready(function() {
 	 
                        $('ul#myTab li').click(function() {                             
                            $('li').removeClass('active');                            
                            $(this).addClass('active');
                          
			//CargarSeccion(pagina);
									
									
                        });
                        
                    });
                                        </script>
                                   

<br style="clear: both;" />
<div id="update_div">
 &nbsp;
</div>

<form>
<input type="hidden" name="twitter_id" id="twitter_id" value="<?=$q1a['id']?>" />
<input type="hidden" name="page_id" id="page_id" value="1" />
<input type="hidden" name="pass_msg" id="pass_msg" value="<?=$pass_msg?>" />
</form>

<form method="post" action="" name="deltweet" id="deltweet">
 <input type="hidden" name="a" id="a" value="deletetweet" />
 <input type="hidden" name="deltweet_id" id="deltweet_id" value="" />
</form>

<form method="post" action="" name="edittweet" id="edittweet">
 <input type="hidden" name="a" id="a" value="edittweet" />
 <input type="hidden" name="edittweet_id" id="edittweet_id" value="" />
</form>
                         </div>
                        </div>
                    </div>

                </div>
<br style="clear: both;" />
<a href="<?=BASE_LINK_URL?>">Return to main admin screen</a>

<?php
include('fin.menu.php');
//End of valid id
}
 
?>


