<?php
/*
Twando.com Free PHP Twitter Application
http://www.twando.com/
*/

if (!$content_id) {
exit;
}
global $q1a, $pass_msg;

?>

<?php
if ($q1a['id'] == "")  {
 echo mainFuncs::push_response(7);
} else {
  if($_SESSION['perfil']== '2'){
     include('ini.menu.admin.php'); 
    }else{
    include('ini.menu.php');}

//List all options here
?>

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Usuarios</h3>
                        </div>
  
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Crear Usuario </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
               

                                    <form name="form" action="" id="create" method="post" class="form-horizontal form-label-left">
      
                                 <div class="form-group">
                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuario">Nombre de usuario <span class="required">*</span>
                                     </label>
                                     <div class="col-md-6 col-sm-6 col-xs-12">
                                         <input type="text" id="usuario" required="required" name="nom" class="form-control col-md-7 col-xs-12">
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="perfil"> Perfil<span class="required">*</span>
                                     </label>
                                     <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="heard" class="form-control" name="perfil" required>
                                    <option value="">Selecciona..</option>
                                    <option value="2">Administrador</option>
                                    <option value="1">Usuario</option>

                                    </select>  
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="clave">Contraseña<span class="required">*</span>
                                     </label>
                                     <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="hidden" name="grabar" value="si">
                                         <input type="password" id="clave" required="required" name="pass" class="form-control col-md-7 col-xs-12">
                                     </div>
                                 </div>
                                  <div class="form-group">
                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="clave">Repita Contraseña<span class="required">*</span>
                                     </label>
                                     <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="hidden" name="grabar" value="si">
                                         <input type="password" id="clave" required="required" name="pass2" class="form-control col-md-7 col-xs-12">
                                     </div>
                                 </div>

      
                              <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                               
                                                <input type="submit" value="Enviar" class="btn btn-primary" />
                                            </div>
                                        </div>
      </form>    

         <h4 id='loading' ></h4>
            <div id="message"></div>


                         </div>
                        </div>
                    </div>

                </div>
                    <script type="text/javascript">
                      $(document).ready(function (e) {
        $("#create").on('submit', (function (e) {
            e.preventDefault();
            $("#message").empty();
            $('#loading').show();
            $.ajax({
                url: "inc/ajax/procesa_userc.php", // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    $('#loading').hide();
                    $("#message").html(data);
                }
            });
        }));
         });
        </script>
<br style="clear: both;" />
<a href="<?=BASE_LINK_URL?>">Return to main admin screen</a>

<?php
include('fin.menu.php');
//End of valid id
}
 
?>


