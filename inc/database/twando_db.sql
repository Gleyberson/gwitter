-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-02-2016 a las 11:25:20
-- Versión del servidor: 5.5.47-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `twando_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_ap_settings`
--

CREATE TABLE IF NOT EXISTS `tw_ap_settings` (
  `id` varchar(10) NOT NULL,
  `consumer_key` varchar(255) NOT NULL,
  `consumer_secret` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_authed_users`
--

CREATE TABLE IF NOT EXISTS `tw_authed_users` (
  `id` varchar(48) NOT NULL,
  `oauth_token` varchar(255) NOT NULL,
  `oauth_token_secret` varchar(255) NOT NULL,
  `profile_image_url` text NOT NULL,
  `screen_name` varchar(32) NOT NULL,
  `followers_count` int(11) NOT NULL,
  `friends_count` int(11) NOT NULL,
  `auto_follow` tinyint(1) NOT NULL DEFAULT '0',
  `auto_unfollow` tinyint(1) NOT NULL DEFAULT '0',
  `auto_dm` tinyint(1) NOT NULL DEFAULT '0',
  `auto_dm_msg` text NOT NULL,
  `log_data` tinyint(1) NOT NULL DEFAULT '0',
  `fr_fw_fp` tinyint(1) NOT NULL DEFAULT '0',
  `last_updated` datetime NOT NULL,
  `statuses_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_cron_logs`
--

CREATE TABLE IF NOT EXISTS `tw_cron_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(48) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `log_text` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `affected_users` longtext NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=106 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_cron_status`
--

CREATE TABLE IF NOT EXISTS `tw_cron_status` (
  `cron_name` varchar(10) NOT NULL DEFAULT '',
  `cron_state` tinyint(1) NOT NULL DEFAULT '0',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cron_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tw_cron_status`
--

INSERT INTO `tw_cron_status` (`cron_name`, `cron_state`, `last_updated`) VALUES
('follow', 0, '0000-00-00 00:00:00'),
('tweet', 0, '2016-02-26 11:25:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_follow_exclusions`
--

CREATE TABLE IF NOT EXISTS `tw_follow_exclusions` (
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `owner_id` varchar(48) NOT NULL,
  `twitter_id` varchar(48) NOT NULL,
  `screen_name` varchar(32) NOT NULL,
  `profile_image_url` text NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`type`,`owner_id`,`twitter_id`),
  KEY `type` (`type`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_fr_429533744`
--

CREATE TABLE IF NOT EXISTS `tw_fr_429533744` (
  `twitter_id` varchar(48) NOT NULL,
  `stp` tinyint(1) NOT NULL DEFAULT '0',
  `ntp` tinyint(1) NOT NULL DEFAULT '0',
  `otp` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`twitter_id`),
  KEY `stp` (`stp`),
  KEY `ntp` (`ntp`),
  KEY `otp` (`otp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_fw_429533744`
--

CREATE TABLE IF NOT EXISTS `tw_fw_429533744` (
  `twitter_id` varchar(48) NOT NULL,
  `stp` tinyint(1) NOT NULL DEFAULT '0',
  `ntp` tinyint(1) NOT NULL DEFAULT '0',
  `otp` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`twitter_id`),
  KEY `stp` (`stp`),
  KEY `ntp` (`ntp`),
  KEY `otp` (`otp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_lists`
--

CREATE TABLE IF NOT EXISTS `tw_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  `tweet_content` text COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_to_post` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_list_settings`
--

CREATE TABLE IF NOT EXISTS `tw_list_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `all_day` tinyint(1) NOT NULL,
  `frecuencia` int(5) NOT NULL,
  `time_to_post` datetime NOT NULL,
  `fecha_fin` date NOT NULL,
  `activa` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tw_list_settings`
--

INSERT INTO `tw_list_settings` (`id`, `all_day`, `frecuencia`, `time_to_post`, `fecha_fin`, `activa`) VALUES
(1, 0, 1, '2016-02-23 11:22:06', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_scheduled_tweets`
--

CREATE TABLE IF NOT EXISTS `tw_scheduled_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(48) CHARACTER SET latin1 NOT NULL,
  `tweet_content` text CHARACTER SET latin1 NOT NULL,
  `time_to_post` date NOT NULL,
  `fecha_ini` date DEFAULT NULL,
  `fecha_fin` date NOT NULL,
  `time` time NOT NULL,
  `id_user` int(11) NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `time_to_post` (`time_to_post`),
  KEY `time_to_post_2` (`time_to_post`),
  KEY `time_to_post_3` (`time_to_post`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_user_cache`
--

CREATE TABLE IF NOT EXISTS `tw_user_cache` (
  `twitter_id` varchar(48) NOT NULL,
  `screen_name` varchar(32) NOT NULL,
  `actual_name` varchar(64) NOT NULL,
  `profile_image_url` text NOT NULL,
  `followers_count` int(11) NOT NULL DEFAULT '0',
  `friends_count` int(11) NOT NULL DEFAULT '0',
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`twitter_id`),
  KEY `screen_name` (`screen_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_usuarios`
--

CREATE TABLE IF NOT EXISTS `tw_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `perfil` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `tw_usuarios`
--

INSERT INTO `tw_usuarios` (`id`, `username`, `password`, `perfil`) VALUES
(12, 'admin', '21232f297a57a5a743894a0e4a801fc3', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
