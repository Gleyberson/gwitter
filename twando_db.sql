-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-02-2016 a las 08:31:13
-- Versión del servidor: 5.5.47-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `twando_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_ap_settings`
--

CREATE TABLE IF NOT EXISTS `tw_ap_settings` (
  `id` varchar(10) NOT NULL,
  `consumer_key` varchar(255) NOT NULL,
  `consumer_secret` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tw_ap_settings`
--

INSERT INTO `tw_ap_settings` (`id`, `consumer_key`, `consumer_secret`) VALUES
('twando', '8AwF98A1Z8aLQA56vh6zy5B7n', 'dCVjzE4wRO86j71wKHJ7MtiisFNpCGcxod4dk46fXNPl7MNVD7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_authed_users`
--

CREATE TABLE IF NOT EXISTS `tw_authed_users` (
  `id` varchar(48) NOT NULL,
  `oauth_token` varchar(255) NOT NULL,
  `oauth_token_secret` varchar(255) NOT NULL,
  `profile_image_url` text NOT NULL,
  `screen_name` varchar(32) NOT NULL,
  `followers_count` int(11) NOT NULL,
  `friends_count` int(11) NOT NULL,
  `auto_follow` tinyint(1) NOT NULL DEFAULT '0',
  `auto_unfollow` tinyint(1) NOT NULL DEFAULT '0',
  `auto_dm` tinyint(1) NOT NULL DEFAULT '0',
  `auto_dm_msg` text NOT NULL,
  `log_data` tinyint(1) NOT NULL DEFAULT '0',
  `fr_fw_fp` tinyint(1) NOT NULL DEFAULT '0',
  `last_updated` datetime NOT NULL,
  `statuses_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tw_authed_users`
--

INSERT INTO `tw_authed_users` (`id`, `oauth_token`, `oauth_token_secret`, `profile_image_url`, `screen_name`, `followers_count`, `friends_count`, `auto_follow`, `auto_unfollow`, `auto_dm`, `auto_dm_msg`, `log_data`, `fr_fw_fp`, `last_updated`, `statuses_count`) VALUES
('429533744', '429533744-OEpETxCaNYShuJsWKdU0cDreHSW1JZAxjU3cmKyW', 'MSNw1wHFk5qLvrXFjpp6lxezL4bo2EI4NFh5EsumldkMg', 'http://pbs.twimg.com/profile_images/2218253696/IMG00166-20120403-1031_normal.jpg', 'GleybersonR', 44, 103, 0, 0, 0, '', 1, 0, '2016-01-19 13:29:47', 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_cron_logs`
--

CREATE TABLE IF NOT EXISTS `tw_cron_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(48) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `log_text` text NOT NULL,
  `affected_users` longtext NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

--
-- Volcado de datos para la tabla `tw_cron_logs`
--

INSERT INTO `tw_cron_logs` (`id`, `owner_id`, `type`, `log_text`, `affected_users`, `last_updated`) VALUES
(9, '429533744', 2, 'Tweet "wetgwegw" posted succesfully.', '', '2016-01-21 10:30:02'),
(10, '429533744', 2, 'Tweet "dsgsdgs" posted succesfully.', '', '2016-01-21 14:25:01'),
(11, '429533744', 2, 'Tweet "sdghsdgsd" posted succesfully.', '', '2016-01-21 14:25:02'),
(12, '429533744', 2, 'Tweet "sfdghsdgas" posted succesfully.', '', '2016-01-21 14:25:03'),
(13, '429533744', 2, 'Tweet "sdfsgtwgw" posted succesfully.', '', '2016-01-22 10:30:03'),
(14, '429533744', 2, 'Tweet "wefwe" posted succesfully.', '', '2016-01-22 10:30:05'),
(15, '429533744', 2, 'Tweet "sdgfsa" posted succesfully.', '', '2016-01-22 11:05:02'),
(16, '429533744', 2, 'Tweet "gjdgdzg" posted succesfully.', '', '2016-01-22 11:30:02'),
(17, '429533744', 2, 'Tweet "werhgwreh" posted succesfully.', '', '2016-01-29 11:40:02'),
(18, '429533744', 2, 'Tweet "gruiz" posted succesfully.', '', '2016-01-29 11:50:05'),
(19, '429533744', 2, 'Tweet "tweet cron :D" posted succesfully.', '', '2016-01-29 14:05:09'),
(20, '429533744', 2, 'Tweet "tweet cron :D" posted succesfully.', '', '2016-01-30 14:05:02'),
(21, '429533744', 2, 'Tweet "hola soy bata" posted succesfully.', '', '2016-02-02 11:10:01'),
(22, '429533744', 2, 'Tweet "eeeee" posted succesfully.', '', '2016-02-02 11:30:01'),
(23, '429533744', 2, 'Tweet "eeeeee" posted succesfully.', '', '2016-02-02 11:30:02'),
(24, '429533744', 2, 'Tweet "teeeeeee" posted succesfully.', '', '2016-02-02 11:40:01'),
(25, '429533744', 2, 'Tweet "eefewfewfweewewf" posted succesfully.', '', '2016-02-02 11:45:01'),
(26, '429533744', 2, 'Tweet "sfddfgweg" posted succesfully.', '', '2016-02-02 12:05:02'),
(27, '429533744', 2, 'Tweet "hi" posted succesfully.', '', '2016-02-02 13:50:02'),
(28, '429533744', 2, 'Tweet "higfgfgg" posted succesfully.', '', '2016-02-02 13:50:02'),
(29, '429533744', 2, 'Tweet "gruiz" posted succesfully.', '', '2016-02-03 09:00:01'),
(30, '429533744', 2, 'Tweet "gruiz" failed to post.', '', '2016-02-03 09:55:02'),
(31, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 13:16:53'),
(32, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 13:30:04'),
(33, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 13:35:01'),
(34, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 13:45:03'),
(35, '429533744', 2, 'Tweet "prog 2" failed to post.', '', '2016-02-04 13:50:02'),
(36, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 13:55:02'),
(37, '429533744', 2, 'Tweet "prog 2" failed to post.', '', '2016-02-04 14:00:02'),
(38, '429533744', 2, 'Tweet "prog 1" posted succesfully.', '', '2016-02-04 14:03:46'),
(39, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 14:05:02'),
(40, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 14:15:02'),
(41, '429533744', 2, 'Tweet "prog 11" failed to post.', '', '2016-02-04 14:20:02'),
(42, '429533744', 2, 'Tweet "prog 1695319557670584320" posted succesfully.', '', '2016-02-04 14:24:37'),
(43, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 14:25:02'),
(44, '429533744', 2, 'Tweet "prog 1" posted succesfully.', '', '2016-02-04 14:40:07'),
(45, '429533744', 2, 'Tweet "prog 1" failed to post.', '', '2016-02-04 14:45:02'),
(46, '429533744', 2, 'Tweet "prog 1code=400" failed to post.', '', '2016-02-04 14:50:02'),
(47, '429533744', 2, 'Tweet "prog 1code=400" failed to post.', '', '2016-02-05 07:18:44'),
(48, '429533744', 2, 'Tweet "prog 1code=400" failed to post.', '', '2016-02-05 07:20:02'),
(49, '429533744', 2, 'Tweet "prog 1code=200" posted succesfully.', '', '2016-02-05 08:32:13'),
(50, '429533744', 2, 'Tweet "prog 1code=400" failed to post.', '', '2016-02-05 08:35:02'),
(51, '429533744', 2, 'Tweet "prog 1code=400" failed to post.', '', '2016-02-05 08:40:02'),
(52, '429533744', 2, 'Tweet "prog 1code=200" posted succesfully.', '', '2016-02-05 09:07:55'),
(53, '429533744', 2, 'Tweet "prog 1code=200" posted succesfully.', '', '2016-02-05 09:15:07'),
(54, '429533744', 2, 'Tweet "prog 1code=200" posted succesfully.', '', '2016-02-05 09:20:06'),
(55, '429533744', 2, 'Tweet "ahora si funciona " posted succesfully.', '', '2016-02-05 09:25:05'),
(56, '429533744', 2, 'Tweet "prog 2" posted succesfully.', '', '2016-02-05 13:50:02'),
(57, '429533744', 2, 'Tweet "ahora si funciona " posted succesfully.', '', '2016-02-06 09:25:02'),
(58, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-12 10:35:40'),
(59, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-12 10:35:55'),
(60, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:47:01'),
(61, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:48:01'),
(62, '429533744', 2, 'Tweet "" posted succesfully.', '', '2016-02-19 08:49:05'),
(63, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:50:01'),
(64, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:51:02'),
(65, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:52:01'),
(66, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:53:01'),
(67, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:54:02'),
(68, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:55:01'),
(69, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:56:01'),
(70, '429533744', 2, 'Tweet "" failed to post.', '', '2016-02-19 08:57:01'),
(71, '429533744', 2, 'Tweet "" posted succesfully.', '', '2016-02-19 08:58:03'),
(72, '429533744', 2, 'Tweet "excelente pelicula" posted succesfully.', '', '2016-02-19 09:38:04'),
(73, '429533744', 2, 'Tweet "cosmo y wanda creen que engaÃ±an" posted succesfully.', '', '2016-02-19 09:48:05'),
(74, '429533744', 2, 'Tweet "entrenamiento del madrid" posted succesfully.', '', '2016-02-19 09:58:03'),
(75, '429533744', 2, 'Tweet "http://ohdios.org/encuentra-las-7-diferencias-qYs" posted succesfully.', '', '2016-02-19 10:13:03'),
(76, '429533744', 2, 'Tweet "al fin funciona " posted succesfully.', '', '2016-02-19 10:43:03'),
(77, '429533744', 2, 'Tweet "6 stars mob" posted succesfully.', '', '2016-02-19 11:13:04'),
(78, '429533744', 2, 'Tweet "excelente pelicula" posted succesfully.', '', '2016-02-19 11:43:06'),
(79, '429533744', 2, 'Tweet "cosmo y wanda creen que engaÃ±an" posted succesfully.', '', '2016-02-19 12:13:06'),
(80, '429533744', 2, 'Tweet "entrenamiento del madrid" posted succesfully.', '', '2016-02-19 12:43:05'),
(81, '429533744', 2, 'Tweet "http://ohdios.org/encuentra-las-7-diferencias-qYs" posted succesfully.', '', '2016-02-19 13:13:02'),
(82, '429533744', 2, 'Tweet "al fin funciona " posted succesfully.', '', '2016-02-19 13:43:04'),
(83, '429533744', 2, 'Tweet "6 stars mob" posted succesfully.', '', '2016-02-19 14:13:04'),
(84, '429533744', 2, 'Tweet "excelente pelicula" posted succesfully.', '', '2016-02-19 14:43:03'),
(85, '429533744', 2, 'Tweet "cosmo y wanda creen que engaÃ±an" posted succesfully.', '', '2016-02-19 15:13:04'),
(86, '429533744', 2, 'Tweet "entrenamiento del madrid" posted succesfully.', '', '2016-02-19 15:43:03'),
(87, '429533744', 2, 'Tweet "http://ohdios.org/encuentra-las-7-diferencias-qYs" posted succesfully.', '', '2016-02-19 16:13:02'),
(88, '429533744', 2, 'Tweet "al fin funciona " posted succesfully.', '', '2016-02-19 16:43:02'),
(89, '429533744', 2, 'Tweet "6 stars mob" posted succesfully.', '', '2016-02-19 17:13:04'),
(90, '429533744', 2, 'Tweet "excelente pelicula" posted succesfully.', '', '2016-02-19 17:43:03'),
(91, '429533744', 2, 'Tweet "cosmo y wanda creen que engaÃ±an" posted succesfully.', '', '2016-02-19 18:13:03'),
(92, '429533744', 2, 'Tweet "entrenamiento del madrid" posted succesfully.', '', '2016-02-19 18:43:03'),
(93, '429533744', 2, 'Tweet "http://ohdios.org/encuentra-las-7-diferencias-qYs" posted succesfully.', '', '2016-02-19 19:13:03'),
(94, '429533744', 2, 'Tweet "al fin funciona " posted succesfully.', '', '2016-02-19 19:43:02'),
(95, '429533744', 2, 'Tweet "6 stars mob" posted succesfully.', '', '2016-02-19 20:13:03'),
(96, '429533744', 2, 'Tweet "excelente pelicula" posted succesfully.', '', '2016-02-19 20:43:03'),
(97, '429533744', 2, 'Tweet "cosmo y wanda creen que engaÃ±an" posted succesfully.', '', '2016-02-19 21:13:03'),
(98, '429533744', 2, 'Tweet "entrenamiento del madrid" posted succesfully.', '', '2016-02-19 21:43:03'),
(99, '429533744', 2, 'Tweet "http://ohdios.org/encuentra-las-7-diferencias-qYs" posted succesfully.', '', '2016-02-19 22:13:03'),
(100, '429533744', 2, 'Tweet "al fin funciona " posted succesfully.', '', '2016-02-19 22:43:03'),
(101, '429533744', 2, 'Tweet "6 stars mob" posted succesfully.', '', '2016-02-21 07:46:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_cron_status`
--

CREATE TABLE IF NOT EXISTS `tw_cron_status` (
  `cron_name` varchar(10) NOT NULL DEFAULT '',
  `cron_state` tinyint(1) NOT NULL DEFAULT '0',
  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cron_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tw_cron_status`
--

INSERT INTO `tw_cron_status` (`cron_name`, `cron_state`, `last_updated`) VALUES
('follow', 0, '0000-00-00 00:00:00'),
('tweet', 0, '2016-02-22 08:31:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_follow_exclusions`
--

CREATE TABLE IF NOT EXISTS `tw_follow_exclusions` (
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `owner_id` varchar(48) NOT NULL,
  `twitter_id` varchar(48) NOT NULL,
  `screen_name` varchar(32) NOT NULL,
  `profile_image_url` text NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`type`,`owner_id`,`twitter_id`),
  KEY `type` (`type`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_fr_429533744`
--

CREATE TABLE IF NOT EXISTS `tw_fr_429533744` (
  `twitter_id` varchar(48) NOT NULL,
  `stp` tinyint(1) NOT NULL DEFAULT '0',
  `ntp` tinyint(1) NOT NULL DEFAULT '0',
  `otp` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`twitter_id`),
  KEY `stp` (`stp`),
  KEY `ntp` (`ntp`),
  KEY `otp` (`otp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_fw_429533744`
--

CREATE TABLE IF NOT EXISTS `tw_fw_429533744` (
  `twitter_id` varchar(48) NOT NULL,
  `stp` tinyint(1) NOT NULL DEFAULT '0',
  `ntp` tinyint(1) NOT NULL DEFAULT '0',
  `otp` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`twitter_id`),
  KEY `stp` (`stp`),
  KEY `ntp` (`ntp`),
  KEY `otp` (`otp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_lists`
--

CREATE TABLE IF NOT EXISTS `tw_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  `tweet_content` text COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_to_post` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `tw_lists`
--

INSERT INTO `tw_lists` (`id`, `owner_id`, `tweet_content`, `id_user`, `imagen`, `time_to_post`) VALUES
(5, '429533744', 'cosmo y wanda creen que engaÃ±an', 2, '12631486_1677821032478040_9166877257015689015_n.png', '2016-02-19 21:12:44'),
(6, '429533744', 'entrenamiento del madrid', 2, '10405334_952401268128258_245301465893716488_n.jpg', '2016-02-19 21:42:44'),
(7, '429533744', 'http://ohdios.org/encuentra-las-7-diferencias-qYs', 2, 'wtm_od_56c5f62d413f2.jpg', '2016-02-19 22:12:44'),
(8, '429533744', 'al fin funciona ', 2, 'original_od_56c4d773b4082.png', '2016-02-19 22:42:44'),
(9, '429533744', '6 stars mob', 2, '6-star-fodder-chart-by-buttbuttbutt.png', '2016-02-19 23:12:44'),
(10, '429533744', 'excelente pelicula', 2, '12717818_1021139501257640_1770578613264141329_n.png', '2016-02-19 20:42:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_list_settings`
--

CREATE TABLE IF NOT EXISTS `tw_list_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `all_day` tinyint(1) NOT NULL,
  `frecuencia` int(5) NOT NULL,
  `time_to_post` datetime NOT NULL,
  `fecha_fin` date NOT NULL,
  `activa` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tw_list_settings`
--

INSERT INTO `tw_list_settings` (`id`, `all_day`, `frecuencia`, `time_to_post`, `fecha_fin`, `activa`) VALUES
(1, 0, 30, '2016-02-19 23:42:44', '2016-02-20', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_scheduled_tweets`
--

CREATE TABLE IF NOT EXISTS `tw_scheduled_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(48) CHARACTER SET latin1 NOT NULL,
  `tweet_content` text CHARACTER SET latin1 NOT NULL,
  `time_to_post` date NOT NULL,
  `fecha_ini` date DEFAULT NULL,
  `fecha_fin` date NOT NULL,
  `time` time NOT NULL,
  `id_user` int(11) NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `time_to_post` (`time_to_post`),
  KEY `time_to_post_2` (`time_to_post`),
  KEY `time_to_post_3` (`time_to_post`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tw_scheduled_tweets`
--

INSERT INTO `tw_scheduled_tweets` (`id`, `owner_id`, `tweet_content`, `time_to_post`, `fecha_ini`, `fecha_fin`, `time`, `id_user`, `imagen`) VALUES
(1, '429533744', 'twer23', '0000-00-00', '2016-02-28', '2016-02-28', '13:02:00', 2, 'Captura de pantalla de 2016-02-21 11:23:38.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_user_cache`
--

CREATE TABLE IF NOT EXISTS `tw_user_cache` (
  `twitter_id` varchar(48) NOT NULL,
  `screen_name` varchar(32) NOT NULL,
  `actual_name` varchar(64) NOT NULL,
  `profile_image_url` text NOT NULL,
  `followers_count` int(11) NOT NULL DEFAULT '0',
  `friends_count` int(11) NOT NULL DEFAULT '0',
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`twitter_id`),
  KEY `screen_name` (`screen_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tw_usuarios`
--

CREATE TABLE IF NOT EXISTS `tw_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `tw_usuarios`
--

INSERT INTO `tw_usuarios` (`id`, `username`, `password`) VALUES
(1, '', ''),
(2, 'gruiz', '325693'),
(6, '', '123'),
(7, 'gruizz', '123');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
